import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve any parameter or the courseId passed via the URL
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course

	//an object with methods to redirect the user
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);

	const handleQuantityChange = (event) => {
	setQuantity(event.target.value);

  };
const order = async () => {
  try {
    const response = await  fetch(`${process.env.REACT_APP_API_URL}/order/new`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
      }),
    });

    const data = await response.json();

    console.log(data.message);
    console.log(productId, quantity);

    if (data) {
      Swal.fire({
        title: 'Successfully ordered',
        icon: 'success',
        text: 'You have successfully ordered this product.',
      });

      navigate('/product');
    } else {
      Swal.fire({
        title: 'Something went wrong',
        icon: 'error',
        text: 'Please try again.',
      });
    }
  } catch (error) {
    console.error('Error:', error);
    Swal.fire({
      title: 'Error',
      icon: 'error',
      text: 'An error occurred. Please try again later.',
    });
  }
};

	useEffect(()=> {

		console.log(productId);

 fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			//setQuantity(data.quantity)

		});

	}, [productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							   <Form.Group controlId="quantity">
					                <Form.Label>Quantity:</Form.Label>
					                <Form.Control
					                  type="number"
					                  value={quantity}
					                  onChange={handleQuantityChange}
					                  min={1} // Set minimum value to 1 or your desired minimum
					                />
					              </Form.Group>

							{ user.id !== null ? 
									<Button variant="primary" block="true" onClick={() => order(productId)}>Order</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/user/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}