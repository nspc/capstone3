import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

export default function Products() {

	const { user } = useContext(UserContext);


	const [products, setProducts] = useState([]);
	const [activeProducts, setActiveProducts] = useState([]);

	console.log(user);
	
	const fetchData = () => {
 fetch(`${process.env.REACT_APP_API_URL}/product/all`,{
			headers: { 
				'Content-Type': 'application/json',         
				Authorization: `Bearer ${localStorage.getItem('token')}`      
		}   
		  })
		.then(res => res.json())
		.then(data => {
		    
		    console.log(data);
		    setProducts(data);

		});
	}

	const fetchActiveData = () => {
 fetch(`${process.env.REACT_APP_API_URL}/product`,{
			headers: { 
				'Content-Type': 'application/json',         
				Authorization: `Bearer ${localStorage.getItem('token')}`      
		}   
		  })
		.then(res => res.json())
		.then(data => {
		    
		    console.log(data);
		    setActiveProducts(data);

		});
	}
    useEffect(() => {

		fetchData()
		fetchActiveData()

    }, []);

	// The "map" method loops through the individual course objects in our array and returns a component for each course
    // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
    // Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp
    // })
    console.log(activeProducts)
	return(
		<>
            {
            	(user.isAdmin === true) ?
            		<AdminView productsData={products} fetchData={fetchData} />

            		:

            		<UserView productsData={ activeProducts} fetchData={fetchActiveData} />

        	}
        </>
	)
}











