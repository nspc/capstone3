
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import FeaturedProducts from '../components/FeaturedProducts';

export default function Home() {

	const data = {
        title: "KPOP Noona Haven Store",
        content: "For the love of KPOP",
        destination: "/product",
        label: "Shop Now!"
    }
    
	return (
		<>
			<Banner data={data}/>
			<FeaturedProducts />
			<Highlights />
			

		</>
	)
}