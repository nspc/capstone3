import { useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';


import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar className="customColor-Pink" expand="lg">
			<Container fluid>
			    <Navbar.Brand as={Link} to="/">KPOP Noona Haven</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="ms-auto">
				        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
				        <Nav.Link as={NavLink} to="/product" exact>Shop</Nav.Link>
				        {(user.id !== null) ? 

								user.isAdmin 
								?
								<>
									{/*<Nav.Link as={Link} to="/product/add">Add Product</Nav.Link>*/}
									<Nav.Link as={Link} to="/user/logout">Logout</Nav.Link>
								</>
								:
								<>
									<Nav.Link as={Link} to="/user/profile">Profile</Nav.Link>
									<Nav.Link as={Link} to="/user/logout">Logout</Nav.Link>
								</>
							: 
								<>
									<Nav.Link as={Link} to="/user/login">Login</Nav.Link>
									<Nav.Link as={Link} to="/user/register">Register</Nav.Link>
								</>
						}
				    </Nav>
			    </Navbar.Collapse>
			</Container>
		</Navbar>
		)
}