import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';


export default function UserView({productsData}) {

  const [products, setProducts] = useState([])

useEffect(() => {
  if (Array.isArray(productsData)) {
    const productsArr = productsData
      .filter(product => product.isActive === true)
      .map(product => (
        <ProductCard productProp={product} key={product._id} />
      ));

    setProducts(productsArr);
  }
}, [productsData]);


  return(
    <>
      <ProductSearch />
      { products }
    </>
    )
}