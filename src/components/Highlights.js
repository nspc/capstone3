import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>One Stop Shop for KPOP fans</h2>
                        </Card.Title>
                        <Card.Text>
                            This shop has a variety of KPOP Albums and Merchandise. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2> Open for Resellers</h2>
                        </Card.Title>
                        <Card.Text>
                         We are passionate on helping a fellow KPOP fan to have a business that she/he love. Be a reseller. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Register to get perks</h2>
                        </Card.Title>
                        <Card.Text>
                            Be a member of a growing community of KPOP fans and get perks from us!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
		)
}