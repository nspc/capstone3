import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProduct({ product, fetchData }) {

	//state for courseId for the fetch URL
	const [productId, setProductId] = useState('');

	//Forms state
	//Add state for the forms of course
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	//state for editCourse Modals to open/close
	const [showEdit, setShowEdit] = useState(false)

	//function for opening the modal
	const openEdit = (productId) => {
		//to still get the actual data from the form
 fetch(`${process.env.REACT_APP_API_URL}/product/${ productId }`)
		.then(res => res.json())
		.then(data => {
			//populate all the input values with course info that we fetched
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

		//Then, open the modal
		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName('')
		setDescription('')
		setPrice(0)
	}


	//function to update the product
		const editProduct = (e, productId) => {
			e.preventDefault();

 fetch(`${process.env.REACT_APP_API_URL}/product/${ productId }`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {
					Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: 'Product Successfully Updated'
					})
					closeEdit();
					fetchData();
					
				} else {
					Swal.fire({
						title: 'Error!',
						icon: 'error',
						text: 'Please try again'
					})
					closeEdit();
					fetchData();
				}
			})
		}


	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}> Edit </Button>

		{/*Edit Modal Forms*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text" 
								value={name} 
								onChange={e => setName(e.target.value)} 
								required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text" 
								value={description} 
								onChange={e => setDescription(e.target.value)} 
								required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number" 
								value={price} 
								onChange={e => setPrice(e.target.value)} 
								required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
		)
}