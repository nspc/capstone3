import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({productProp}) {

	// Checks to see if the data was successfully passed
	// console.log(props);
	// Every component recieves information in a form of an object
	// console.log(typeof props);

	// Deconstruct the course properties into their own variables
	const { _id, name, description, price} = productProp;


	return (
		<Card className="mt-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
		        <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
		)
}


ProductCard.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific "shape"
    product: PropTypes.shape({
        // Define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}